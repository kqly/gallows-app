import React, { PureComponent } from 'react';
import classes from './App.css';

import GameBuilder from './components/GameBuilder/GameBuilder';
import HelloScreen from './components/HelloScreen/HelloScreen';

import Background from './assets/hang-alien-min/Background-min.png';

class App extends PureComponent {
  state = {
    word : '',
    wordStatus : 0,
    error : ''
  }

  wordChangeHandler = (ww) => {
    //console.log(ww.toUpperCase())
    let f = 0;
    let uniqueLett = [];
    ww = ww.toUpperCase();
    ww.split('').map(x => {
      let chCode = x.charCodeAt('0');
      if (chCode >= 65 && chCode <= 90) f = 0;
      else f = 1;
      if (x !== ww[0] && x !== ww[ww.length-1]) uniqueLett.push(x);
      //console.log(x,ww[0],ww[ww.length-1],uniqueLett)
      return x;
    });
    if (ww.length < 3 ) f = 0;
    if (uniqueLett.length === 0) f = 1;
    //console.log(f, uniqueLett)
    if (f === 0) this.setState({ word : ww.toUpperCase(), wordStatus : 1, error : '' });
    else this.setState({ error : 'illegal value' });
  }

  chooseRandomWord = () => {
    let data = require('./dict.json');
    //console.log(data.length)
    let rand = Math.floor(Math.random() * (data.length));
    //console.log(data,rand,data[rand])
    this.setState({ word : data[rand], wordStatus : 1, error : '' });
  }

  reloadGameHandler = () => {
    this.setState({ wordStatus : 0});
  }

  render() {
    //console.log("Render app.js\nWord= ",this.state.word)
    let gameAction = null;
    let helloScreen = null;

     if (this.state.wordStatus) gameAction = (<GameBuilder reload={this.reloadGameHandler} word={this.state.word}/>);
     else helloScreen = (<HelloScreen randomW={this.chooseRandomWord} changed={this.wordChangeHandler} error={this.state.error}/>);

    return (
      <div className={classes.container}>
        <div>
        <img src={Background} alt="backgroud"/>       
        </div>
        <div className={classes.center}>
          {helloScreen}
        </div>
        <div className={classes.game} >
          {gameAction}
        </div>
      </div>
    );
  }
}

export default App;
