import React from 'react';
import classes from './LetterKey.css';

const LetterKey = (props) => {
  //console.log(classes1.letter)
  let cllName = '';
  if (props.className1 === 'letter') cllName = classes.letter;
  else if (props.className1 === 'red') cllName = classes.red;
  else if (props.className1 === 'green') cllName = classes.green;
  return (
    <p className={cllName} onClick={props.clicked}>{props.letter} </p>
  );
}

export default LetterKey;