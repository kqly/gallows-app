import React, { PureComponent } from 'react';
import LetterKey from './LetterKey/LetterKey';


class KeyBoard extends PureComponent {
  state = {
    charsState : []
  }

  componentDidMount(){
    //console.log(this.props.clicked)
    let chars = [];
    for(let i=0;i<26;i++){
      chars.push((10+i).toString(36).toUpperCase());
      } 

    function LtKey (...args){
      this.letter = args[0];
      this.clName = args[1];
    }
    
    const chars1 = chars.map((x) => {
      let clName = "letter";
      if ((this.props.keywordHidden[0] === x) || (this.props.keywordHidden[this.props.keywordHidden.length-1] === x)){
        clName = "green";
      }
      //console.log(x,this.props.keywordHidden[this.props.keywordHidden.length-1])
      return new LtKey(x,clName);
    });

    this.setState({ charsState : chars1 });
  }

  render() {
    return (
      this.state.charsState.map((letterObj, index) => {
      if (letterObj.letter === this.props.clickedLetter)
        if (this.props.isKeyLetter === 0) letterObj.clName = 'red';
        else letterObj.clName = 'green';
      //console.log(letter.letter,letter.clName)
      let clickedFunc = () => this.props.clicked(letterObj.letter);
      if (letterObj.clName === 'red' || letterObj.clName === 'green') clickedFunc = () => (0);
        return(
          <LetterKey
          className1={letterObj.clName}
          letter={letterObj.letter}
          key={index}
          clicked={clickedFunc}
          /> 
        );
      })
    );
  } 
}

export default KeyBoard;
