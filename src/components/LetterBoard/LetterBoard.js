import React from 'react'
import classes from './LetterBoard.css'

const LetterBoard = (props) => {
      return(
      <>
        <p className={classes.LtBoard}>{props.word}</p>
      </>
      );
} 

export default LetterBoard;