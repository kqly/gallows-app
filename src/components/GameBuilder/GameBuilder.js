import React, { PureComponent } from 'react';
import classes from './GameBuilder.css';

import LetterBoard from '../LetterBoard/LetterBoard';
import KeyBoard from '../KeyBoard/KeyBoard';
import Gallows from '../Gallows/Gallows';

import WindoWin from '../../assets/hang-alien-min/Win-min.png';
import WindoLost from '../../assets/hang-alien-min/Lost-min.png';


class GameBuilder extends PureComponent {
  state = {
    keyword : this.props.word,
    keywordHidden : '',
    clickedLetter : '',
    isKeyLetter : 0,
    rounds : 7,
    isWin : 0,
    isLose : 0,
  }

  componentWillMount(){
    //console.log('[GameBuilder.js] Inside componentWillMount()',this.state.keyword[0])
    const fistLetter = this.state.keyword[0];
    const lastLetter = this.state.keyword[this.state.keyword.length-1];
        
    let word = this.state.keyword.split('').map(x => {
      if (x === fistLetter || x === lastLetter) return x;
      else return ('_');
    });
    this.setState({ keywordHidden : word });
  }

  letterClickedHandler = (clLetter) =>{
    let flag = 0
    this.setState({ isKeyLetter : 0 })
    let word = this.state.keyword.split('').map((keyLetter, id) => {
      if (keyLetter === clLetter){
        flag = 1;
        this.setState({ isKeyLetter : 1 });
        return keyLetter;
      }
      else {
        return this.state.keywordHidden[id];       
      }
    });

    if (flag === 0) this.setState({ rounds : this.state.rounds-1 });
    this.setState({ keywordHidden : word, clickedLetter : clLetter });  
    //console.log(this.state.rounds)  
    }

  gameProcess = () => {
    if (JSON.stringify(this.state.keyword.split('')) === JSON.stringify(this.state.keywordHidden) ){
      //console.log('You are win!')
      this.setState({isWin:1});
    } 
    else if (this.state.rounds === 0) this.setState({isLose:1});
    //console.log(JSON.stringify(this.state.keyword.split('')), JSON.stringify(this.state.keywordHidden))
  }

  render() {
    let content = null;
    if (this.state.isWin) {
      content = (
      <>
        <LetterBoard word={this.state.keyword}/>
        <div className={classes.window}>
          <img src={WindoWin} alt="WindowWin"/>
          <div className={classes.btnreload}>
            <button onClick={this.props.reload}>TRY AGAIN</button>
          </div>
        </div>
      </>
      );
    }
    else if (this.state.isLose){
      content =(
      <>
        <LetterBoard word={this.state.keyword}/>
        <div className={classes.window}>
          <img src={WindoLost} alt="WindowLost"/>
          <div className={classes.btnreload}>
            <button onClick={this.props.reload}>TRY AGAIN</button>
          </div>
        </div>
      </>
      );
    }
    else{
      content=(
      <>
        <div className={classes.LetterBoard}>
        <LetterBoard word={this.state.keywordHidden}/>
        </div>
        <div className={classes.divb}>
        <KeyBoard className={classes.dvikeybord} keywordHidden={this.state.keywordHidden} clickedLetter={this.state.clickedLetter} isKeyLetter={this.state.isKeyLetter } clicked={this.letterClickedHandler}/>
        </div>
      </>
      );
    }

    //console.log("win",content)
    this.gameProcess();
    return (
      <div className={classes.container}>
      <Gallows rounds={this.state.rounds}/>
        {content}
      </div>
    );
  }
}

export default GameBuilder;
