import React, { PureComponent } from 'react';
import classes from './HelloScreen.css';

import startwindow from '../../assets/hang-alien-min/Start-window-min.png';

export default class HelloScreen extends PureComponent {
  state = {
    newW : ''
  }

  neWChangeHandler = (event) => {
    this.setState({newW:event.target.value});
  }

  render() {
    //console.log(this.state.newW)
    return (
      <>
        <img src={startwindow} alt="startwindow"/>
        <div className={classes.ellementPosition}>
          <input type="text" onChange={this.neWChangeHandler} placeholder="Input your own word here" maxLength="11" autoFocus/>
          <button onClick={()=>this.props.changed(this.state.newW)}>Own Word</button>
          <button onClick={this.props.randomW}>Random Word</button>
          <p className={classes.errorMess}>{this.props.error}</p>
        </div>
      </>
    );
  }
}

