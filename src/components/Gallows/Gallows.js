import React, { PureComponent } from 'react';
import classes from './Gallows.css';

import gallow1 from '../../assets/hang-alien-min/Gallows-part1-min.png';
import gallow2 from '../../assets/hang-alien-min/Gallows-part2-min.png';
import gallow3 from '../../assets/hang-alien-min/Robot-head-min.png';
import gallow4 from '../../assets/hang-alien-min/Robot-body-min.png';
import gallow5 from '../../assets/hang-alien-min/Robot-left-hand-min.png';
import gallow6 from '../../assets/hang-alien-min/Robot-right-hand-min.png';
import gallow7 from '../../assets/hang-alien-min/Robot-left-leg-min.png';
import gallow8 from '../../assets/hang-alien-min/Robot-right-leg-min.png';

class Gallows extends PureComponent {
    render() {
    let imgClasses = [gallow1,gallow2,gallow3,gallow4,gallow5,gallow6,gallow7,gallow8];
    //console.log(imgClasses)

    const recurs = (x,n) => {
        if (n >= 0)
        return (
        <>
          {recurs(x,n-1)}
          <div className={classes.l1} key={Math.random()}>
            <img src={x[n]} alt='gallow-part' />
          </div>
        </>
        );
    }
    
    return(
        recurs(imgClasses, 7-this.props.rounds));
    }
}   

export default Gallows;
